//
//  Commit+CoreDataClass.swift
//  HWS-Project38
//
//  Created by Patryk Pekala on 07.02.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Commit)
public class Commit: NSManagedObject {
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
        print("Init called!")
    }
    
}
