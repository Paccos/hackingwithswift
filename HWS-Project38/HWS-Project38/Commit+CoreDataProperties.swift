//
//  Commit+CoreDataProperties.swift
//  HWS-Project38
//
//  Created by Patryk Pekala on 12.02.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//
//

import Foundation
import CoreData


extension Commit {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Commit> {
        return NSFetchRequest<Commit>(entityName: "Commit")
    }

    @NSManaged public var date: Date
    @NSManaged public var message: String
    @NSManaged public var sha: String
    @NSManaged public var url: String
    @NSManaged public var author: Author

}
