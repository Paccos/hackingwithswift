//
//  DetailViewController.swift
//  HWS-Project38
//
//  Created by Patryk Pekala on 07.02.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var detailLabel: UILabel!
    
    var detailItem: Commit?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let detail = self.detailItem {
            detailLabel.text = detail.message
        }
    }
}
