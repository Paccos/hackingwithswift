//
//  ViewController.swift
//  HWS-Project15
//
//  Created by Patryk Pekala on 14.01.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tap: UIButton!
    
    var imageView: UIImageView!
    var currentAnimation = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView = UIImageView(image: UIImage(named: "penguin"))
        imageView.center = CGPoint(x: 512, y: 384)
        view.addSubview(imageView)
    }


    @IBAction func tapped(_ sender: Any) {
        tap.isHidden.toggle()
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: [], animations: { [unowned self] in
            switch self.currentAnimation {
            case 0:
                self.imageView.transform = CGAffineTransform(scaleX: 2, y: 2)
            case 2:
                self.imageView.transform = CGAffineTransform(translationX: -256, y: -256)
            case 4:
                self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            case 6:
                self.imageView.alpha = 0.1
                self.imageView.backgroundColor = UIColor.green
            case 1, 3, 5, 7:
                self.imageView.transform = CGAffineTransform.identity
                self.imageView.alpha = 1
                self.imageView.backgroundColor = UIColor.clear
            default:
                break
            }
        }) { [unowned self] (finished: Bool) in
            self.tap.isHidden.toggle()
        }
        
        currentAnimation = (currentAnimation + 1) % 8
    }
}

