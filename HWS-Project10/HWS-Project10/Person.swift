//
//  Person.swift
//  HWS-Project10
//
//  Created by Patryk Pekala on 07.01.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import UIKit

class Person: NSObject {
    var name: String
    var image: String
    
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}
