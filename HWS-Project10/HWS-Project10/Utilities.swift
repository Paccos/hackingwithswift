//
//  Utilities.swift
//  HWS-Project10
//
//  Created by Patryk Pekala on 07.01.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import Foundation

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}
