//
//  ViewController.swift
//  HWS-Project31
//
//  Created by Patryk Pekala on 24.01.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var addressBar: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    
    weak var activeWebView: WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDefaultTitle()
        
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addWebView))
        let delete = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteWebView))
        navigationItem.rightBarButtonItems = [delete, add]
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if traitCollection.horizontalSizeClass == .compact {
            stackView.axis = .vertical
        } else {
            stackView.axis = .horizontal
        }
    }

    
    func setDefaultTitle() {
        title = "Multibrowser"
    }
    
    func updateUI(for webView: WKWebView) {
        title = webView.title
        addressBar.text = webView.url?.absoluteString ?? ""
    }
    
    
    @objc func addWebView() {
        let webView = WKWebView()
        webView.navigationDelegate = self
        
        stackView.addArrangedSubview(webView)
        
        let url = URL(string: "https://www.hackingwithswift.com")!
        webView.load(URLRequest(url: url))
        
        webView.layer.borderColor = UIColor.blue.cgColor
        selectWebView(webView)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(webViewTapped))
        recognizer.delegate = self
        webView.addGestureRecognizer(recognizer)
    }
    
    
    @objc func deleteWebView() {
        guard let webView = activeWebView else { return }
        guard let index = stackView.arrangedSubviews.index(of: webView) else { return }
        
        stackView.removeArrangedSubview(webView)
        webView.removeFromSuperview()
        
        guard stackView.arrangedSubviews.count > 0 else {
            setDefaultTitle()
            return
        }
        
        var currentIndex = Int(index)
        
        if currentIndex == stackView.arrangedSubviews.count {
            currentIndex -= 1
        }
        
        if let newSelectedWebView = stackView.arrangedSubviews[currentIndex] as? WKWebView {
            selectWebView(newSelectedWebView)
        }
    }
    
    
    @objc func webViewTapped(_ recognizer: UITapGestureRecognizer) {
        guard let selectedWebView = recognizer.view as? WKWebView else { return }
        
        selectWebView(selectedWebView)
    }
    
    
    func selectWebView(_ webView: WKWebView) {
        stackView.arrangedSubviews.forEach { $0.layer.borderWidth = 0 }
        
        activeWebView = webView
        webView.layer.borderWidth = 3
        
        updateUI(for: webView)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let webView = activeWebView, let address = addressBar.text else { return false }
        guard let url = URL(string: address) else { return false }
        
        webView.load(URLRequest(url: url))
        
        textField.resignFirstResponder()
        return true
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView == activeWebView {
            updateUI(for: webView)
        }
    }
}

