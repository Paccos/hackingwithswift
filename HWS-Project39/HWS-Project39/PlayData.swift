//
//  PlayData.swift
//  HWS-Project39
//
//  Created by Patryk Pekala on 15.02.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import Foundation

class PlayData {
    var allWords = [String]()
    var wordCounts: NSCountedSet!
    private(set) var filteredWords = [String]()
    
    init() {
        guard let path = Bundle.main.path(forResource: "plays", ofType: "txt") else { return }
        guard let plays = try? String(contentsOfFile: path) else { return }
        
        allWords = plays
                    .components(separatedBy: CharacterSet.alphanumerics.inverted)
                    .filter { $0 != "" }
        
        wordCounts = NSCountedSet(array: allWords)
        allWords = (wordCounts.allObjects as! [String])
                    .sorted { wordCounts.count(for: $0) > wordCounts.count(for: $1)}
        
        applyUserFilter("swift")
    }
    
    
    func applyUserFilter(_ input: String) {
        if let userNumber = Int(input) {
            applyFilter { self.wordCounts.count(for: $0) >= userNumber }
        } else {
            applyFilter { $0.range(of: input, options: .caseInsensitive) != nil }
        }
    }
    
    
    func applyFilter(_ filter: (String) -> Bool) {
        filteredWords = allWords.filter(filter)
    }
}
