//
//  PersonCell.swift
//  HWS-Project10
//
//  Created by Patryk Pekala on 07.01.19.
//  Copyright © 2019 Patryk Pekala. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
}
