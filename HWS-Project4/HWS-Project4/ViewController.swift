//
//  ViewController.swift
//  HWS-Project4
//
//  Created by Patryk Pekala on 11.06.18.
//  Copyright © 2018 Patryk Pekala. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    enum ObservedKeyPath: String { 
        case estimatedProgress = "estimatedProgress" 
    }


    // MARK: - Properties
    
    var webView: WKWebView!
    var progressView: UIProgressView!
    var websites = ["apple.com", "hackingwithswift.com"]

    // MARK: - Lifecycle

    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = URL(string: "https://" + websites[0]) else { return }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Open", style: .plain, target: self, action: #selector(openTapped))

        progressView = UIProgressView(progressViewStyle: .default)
        progressView.sizeToFit()
        let progressButton = UIBarButtonItem(customView: progressView)

        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))

        toolbarItems = [progressButton, spacer, refreshButton]
        navigationController?.isToolbarHidden = false

        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }


    // MARK: - User Interaction

    @objc func openTapped() {
        let alertController = UIAlertController(title: "Open Website...", message: nil, preferredStyle: .actionSheet)
        
        websites.forEach { url in
            alertController.addAction(UIAlertAction(title: url, style: .default, handler: openPage))
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        present(alertController, animated: true)
    }

    
    func openPage(action: UIAlertAction) {
        guard let actionTitle = action.title else { return }
        guard let url = URL(string: "https://" + actionTitle) else { return }
        webView.load(URLRequest(url: url))
    }


    // MARK: - KVO

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let path = keyPath else { return }

        if path == ObservedKeyPath.estimatedProgress.rawValue {
            progressView.progress = Float(webView.estimatedProgress)
        }
    }
}



extension ViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }


    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else { 
            decisionHandler(.cancel)
            return
        }

        guard let host = url.host else {
            decisionHandler(.cancel)
            return
        }
    
        for website in websites {
            if host.contains(website) {
                decisionHandler(.allow)
                return
            }
        }

        decisionHandler(.cancel)
    }
}
